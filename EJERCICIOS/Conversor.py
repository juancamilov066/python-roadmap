def main():
    distanciam = float(input("Ingrese la distancia en metros: "))
    unidad = (input("Ingrese el unidad que desea: ").lower())
    print("Su distancia es: " +str(conversor(distanciam, unidad)))

def conversor(distanciam, unidad):
    if unidad == "mi":
        return distanciam * 0.000621371
    elif unidad == "mm":
        return distanciam * 1000
    elif unidad == "km":
        return distanciam / 1000


if __name__ == '__main__':
    main()

