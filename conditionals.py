#Programa de T

celsius = float(input("Ingresa la temperatura en celsius: "))

tipoT= (input("Ingresa el tipo de temperatura: "))


if tipoT.lower() == "fahrenheit":
    fahrenheit = celsius * 1.8 + 32
    print("La temperatura en Fahrenheit es: "+ str(fahrenheit))
elif tipoT.lower() == "kelvin":
    kelvin = celsius + 273.15
    print("La temperatura en Kelvin es: " + str(kelvin))
else :
    print("El valor ingresado no es correcto")

## condicion = int(input('Ingresa un nomero del uno al tres: '))
#
# if (condicion == 1):
#     print('El numero es 1')
# elif (condicion == 2):
#     print('El numero es 2')
# elif (condicion == 3):
#     print('El numero es 3')
# else:
#     print("El numero ingresado es: " + str(condicion))

# Lists, Sets (Conjuntos), Tuples, Diccionarios
# Indice o Index quiere decir posicion

# Lists (Array, Arreglos, Vectores)
# Lists are mutable
# Can have repeated elements
# Each element has an order

# Can allocate multiple data types

x = [7, 4, 2, 4, 6, True, 4.51343, "Mugre", [2,4,5,4]]
y = [4]
#x.append(5) Insertar al final de la lista
#x.insert(3, 100) Inserta en una posicion en particular
#x.clear() Elimina toda la lista
#x.pop() Elimina el ultimo coso de la lista
#y = x[1] Acceder a una posicion de la lista
#print(6 in x) Me dice si un elemento está en la lista

# Sets (Conjuntos)
# Elimina los elementos repetidos
# Opera entre si con las operaciones de conjuntos (union, interseccion,
, diferencia...)

x = {1, 3, 5, 5, 5}
# y = {3, 5, 8}
# print(x.intersection(y))
# print(x.union(y))
# print(x.symmetric_difference(y))

# Tuples
x = (1, 3, 5, 5)
#print(x[1] + x[2])

# Diccionarios (Dataset, set)
# Clave (Key, Llave) - Valor
diccionario = { "nombre": "Julieta", "edad": 34, "list": [1, 2], "diccionario": { "ejemplo": 1 } }
#print(diccionario.keys())
#print(diccionario.values())
#print(diccionario["list"])