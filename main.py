#print("Hola mundo")


# Data Types

# Integer
x = 1
x1 = -400

# Float
#y = 1.1
#y1 = -1.32543435464544

# type() lets me know the type of a variable
# print(type(y))

# int() and float() helps me to change a variable type
# print(int(y1))
# print(float(x))

# Boolean
#z = True
#z1 = False

# String
#f = 'Hola Julieta, te amo'
#f2 = "A Julieta le dicen \"Mugre\""
#f1 = 'I\'m beautiful'

#print(f)

